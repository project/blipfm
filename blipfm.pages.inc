<?php
/**
 * @blipfm.pages.inc
 *
 * Contains functions specific to generating admin and user forms
 */

/**
 * function to show the blip.fm user settings form
 */
function blipfm_user_settings($account) {
  module_load_include('inc', 'blipfm');

  $output = '';

  $output .= drupal_get_form('blipfm_add_account', $account);

  return $output;
}


/**
 * function to build the rows of the account list form
 */
function _blipfm_account_list_row($account) {
  $form['#account'] = $account;

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account['uid'],
  );

  $form['username'] = array(
    '#type' => 'value',
    '#value' => $account['username'],
  );
  
  $form['password'] = array(
    '#type' => 'value',
    '#value' => $account['password'],
  );
  
  $form['delete'] = array(
    '#type' => 'checkbox',
  );
  return $form;
}

/**
 * function to build the account add form
 */
function blipfm_add_account($form_state, $account = NULL) {
  if (empty($account)) {
    global $user;
    $account = $user;
  }

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  $form['username'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('blip.fm user name'),
  );
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => TRUE,
    '#description' => t("Your blip.fm account password")
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add account'),
  );

  return $form;
}

/**
 * function to validate the account add form
 */
function blipfm_add_account_validate($form, &$form_state) {
  module_load_include('inc', 'blipfm');


  $pass = $form_state['values']['password'];
  $name = $form_state['values']['username'];

}

/**
 * function to handle the submission of the account add form
 */
function blipfm_add_account_submit($form, &$form_state) {
  module_load_include('inc', 'blipfm');

  if (!empty($form_state['values']['username'])) {
    blipfm_user_save($form_state['values'], TRUE);
  }
}

/**
 * function to build the blip.fm admin settings form
 */
function blipfm_admin_form() {
  $form['blipfm_api'] = array(
    '#type' => 'textfield',
    '#title' => t('Your blip.fm API key'),
    '#default_value' => variable_get('blipfm_api', ''),
    '#size' => 50,
    '#maxlength' => 128,
    '#description' => t("The API key provided by blip.fm"),
    '#required' => TRUE,
  );
  $form['blipfm_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Your blip.fm API secret'),
    '#default_value' => variable_get('blipfm_secret', ''),
    '#size' => 50,
    '#maxlength' => 128,
    '#description' => t("The API secret provided by blip.fm"),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}