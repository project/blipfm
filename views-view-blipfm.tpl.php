<?php
/**
 * @file views-view-blipfm.tpl.php
 * Template to display the audio player.
 *
 */

$player_url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . drupal_get_path('module', 'blipfm') . '/audio-player/player.swf';
$player_js = 'AudioPlayer.setup("' . $player_url . '", {width: 290  });';

drupal_add_js(drupal_get_path('module', 'blipfm') . '/audio-player/audio-player.js', 'module');
drupal_add_js($player_js, 'inline', 'header');

print_r($this);

$songs = array();
$titles = array();
$artists = array();
foreach ($rows as $row) {
  array_push($songs, $row->blipfm_url);
  array_push($titles, $row->blipfm_title);
  array_push($artists, $row->blipfm_artist);
}

echo '<div id="blipfm_audioplayer_playlist">playlist</div>';
echo '<script type="text/javascript">';
echo 'AudioPlayer.embed("blipfm_audioplayer_playlist", {';
echo 'soundFile: "' . join($songs, ",") . '",';
echo 'titles: "' . join($titles, ",") . '",';
echo 'artists: "' . join($artists, ",") . '"';
echo '});'; 
echo '</script>';


