<?php
/**
 * @views_plugin_style.blipfm.inc
 *
 * Provides a views style plugin that passes row data to a flash audio player.
 * 
 * In the future it would be nice to make this generic and provide options
 * so that any row field could be assigned to url, artist and title, as well
 * as providing options for other audio players.
 */

/**
 * defines the blipfm views plugin style
 */
class views_plugin_style_blipfm extends views_plugin_style {
  
  /**
   * Set default options
   */
  function options(&$options) {
    $options['type'] = '1pixelout';
  }
  
  /**
   * Provide a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */  
  function options_form(&$form, &$form_state) {
    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('Player Type'),
      '#options' => array('1pixelout' => t('1pixelout')),
      '#default_value' => $this->options['type'],
    );
  }

  function render() {
    $rows = array();
    foreach ($this->view->result as $row) {
      // @todo: Include separator as an option.
      $rows[] = $row;
    }
    return theme($this->theme_functions(), $this->view, $this->options, $rows);
  }
}




