<?php

/**
 * @blipfm.views_default.inc
 *
 * The code for the default view(s) provided with the blip.fm module
 */
/**
 * Implementation of hook_default_view_views().
 */
function blipfm_views_default_views() { 
  $view = new view;
  $view->name = 'blipfm';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'blipfm';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'artist' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'artist',
      'table' => 'blipfm',
      'field' => 'artist',
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'title',
      'table' => 'blipfm',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'message' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'message',
      'table' => 'blipfm',
      'field' => 'message',
      'relationship' => 'none',
    ),
    'created_time' => array(
      'label' => '',
      'date_format' => 'time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'created_time',
      'table' => 'blipfm',
      'field' => 'created_time',
      'relationship' => 'none',
    ),
    'url' => array(
      'label' => '',
      'exclude' => 1,
      'id' => 'url',
      'table' => 'blipfm',
      'field' => 'url',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
  ));
  $handler->override_option('sorts', array(
    'created_time' => array(
      'order' => 'DESC',
      'granularity' => 'second',
      'id' => 'created_time',
      'table' => 'blipfm',
      'field' => 'created_time',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'My blip.fm playlist');
  $handler->override_option('items_per_page', 5);
  $handler->override_option('row_options', array(
    'inline' => array(
      'artist' => 'artist',
      'title' => 'title',
    ),
    'separator' => ' - ',
  ));
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->override_option('fields', array(
    'artist' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'artist',
      'table' => 'blipfm',
      'field' => 'artist',
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => '',
      'exclude' => 0,
      'id' => 'title',
      'table' => 'blipfm',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'message' => array(
      'label' => '',
      'exclude' => 1,
      'id' => 'message',
      'table' => 'blipfm',
      'field' => 'message',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Use default',
      ),
    ),
    'created_time' => array(
      'label' => '',
      'date_format' => 'time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'created_time',
      'table' => 'blipfm',
      'field' => 'created_time',
      'relationship' => 'none',
    ),
    'url' => array(
      'label' => '',
      'exclude' => 1,
      'id' => 'url',
      'table' => 'blipfm',
      'field' => 'url',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
  ));
  $handler->override_option('use_more', 1);
  $handler->override_option('block_description', 'blipfm');
  $handler->override_option('block_caching', -1);
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('items_per_page', 20);
  $handler->override_option('path', 'blipfm');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
  ));
  $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
  $handler->override_option('header_format', '1');
  $handler->override_option('header_empty', 1);
  $handler->override_option('items_per_page', 20);
  $handler->override_option('style_plugin', 'views_blipfm');
  $handler->override_option('style_options', array(
    'type' => '1pixelout',
  ));
  $handler->override_option('attachment_position', 'before');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', FALSE);
  $handler->override_option('displays', array(
    'page_1' => 'page_1',
    'default' => 0,
    'block_1' => 0,
  ));
  $views[$view->name] = $view;

  return $views; 
}