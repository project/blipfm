<?php

/**
 * @blipfm.inc
 * Contains common functions used by the blip.fm module.
 */

/**
 * function needed to generate the signature required by the blip.fm API
 */
function _blipfm_hex2b64($str) {
  $raw = '';
  for ($i=0; $i < strlen($str); $i+=2) {
    $raw .= chr(hexdec(substr($str, $i, 2)));
  }
  return base64_encode($raw);
}

/**
 * function needed to generate nonce, which is appended to the API call
 */
function _blipfm_getNonce() {
  return sprintf("%08x", mt_rand());
}

/**
 * Generic function to form the blip.fm API request.  
 */
function _blipfm_call($function, $parameters = array()) {
  foreach($parameters as $key => $value) {
    if(is_array($value)) {
      $parameters[$key] = implode(',', $value);
    }    
  }
  $parameters['api_key'] = variable_get('blipfm_api', 'bdf5543b422687e561aa');
  $parameters['secret'] = variable_get('blipfm_secret', 'Mrd83brG-S!rxeMjf0BcQLw7gYQ2W3p6CCp1Tpd');
  $parameters['timestamp'] = time();
  $parameters['nonce'] = _blipfm_getNonce();
  $parameters['signature'] = _blipfm_generateSignature($parameters, 'GET');
  
  $api_url = 'http://api.blip.fm';
  $api_url .= $function . '?';
  $api_url .= http_build_query($parameters);
  
  $http_result = drupal_http_request($api_url);
  if ($http_result->code == 200) {
	 $data = $http_result->data;
	 return $data;
  }  
}

/**
 * Function that generates the signature that is required by the blip.fm API
 */
function _blipfm_generateSignature ($params, $method = 'GET') {  
  $secret_key = variable_get('blipfm_secret', 'Mrd83brG-S!rxeMjf0BcQLw7gYQ2W3p6CCp1Tpd');
  $strToSign = $method . "\n" . $params['timestamp'] . "\n" . $params['nonce'];

  
  $signature = urlencode(_blipfm_hex2b64(hash_hmac("sha1", $strToSign, $secret_key, false)));
  
  return $signature;
}

/**
 * Function to get the list of user accounts from the blipfm_user table
 */
function blipfm_get_user_accounts($uid) {
  $sql = "SELECT bu.uid, bu.username, bu.password FROM {blipfm_user} bu WHERE bu.uid=%d";
  $args = array($uid);
  $results = db_query($sql, $args);

  $accounts = array();
  while ($account = db_fetch_array($results)) {
    $accounts[$account['username']] = $account;
  }
  return $accounts;
}

/**
 * Function to save the blip.fm user account information
 */
function blipfm_user_save($account = array(), $force_import = FALSE) {
  $account += array(
    'username' => '',
  );
  if (db_result(db_query("SELECT 1 FROM {blipfm_user} WHERE uid = %d AND username = '%s'", $account['uid'], $account['usernamename']))) {
    drupal_write_record('blipfm_user', $account, array('uid', 'username'));  }
  else {
    drupal_write_record('blipfm_user', $account);
  }
}
