
The blip.fm module allows users to download their blip.fm blips to 
a local Drupal install.  That data is made available to views for
display as a block or a page.

Installation
------------
Copy blipfm to your module directory (usually sites/all/modules)
and then enable on the admin modules page.  It is recommended that
you get an API key at http://api.blip.fm however one is provided by
default.  You can set your blip.fm settings at admin/settings/blipfm

You also need to add your blip.fm account info at user/%userid/edit/blipfm

Requirements
------------
Drupal 6.x and PHP 5.1.  NikePlus uses the SimpleXML object.  
Views2 is required.
blip.fm relies on cron to sync data from the blip.fm website.
If cron isn't enabled, or isn't working, you can run cron manually or use poormanscron.

Author
------
Steve Karsch
steve@stevekarsch.com